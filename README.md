# Stream #
The Stream project provides a streaming API for ILE.

A stream consist of an emitter which provides the base data for the stream. The
data can be piped through pipes. Those pipes can do various things

- inspect the data, react on it and push it further down to the next pipe
- drop the data and don't push it to the next pipe
- create a different kind of data and push it to the next pipe
- create multiple data of the same or different kind and push it to the next pipe

And after all the inspecting, reacting and transforming it can be drained in a
sink which is the end of the road for the data. The sink does the end processing
of the data.


## Example ##

1. Emitter : reads blocks of data from a file and passes it down to the next pipe
2. Pipe: calculates MD5 hash of that block of data but drops the data till it has 
         received all blocks and passes the MD5 hash down to the next pipe or sink
3. Sink: writes the hash to a database table

The code could look like this:

```
// setup the stream
stream = stream_create(emitter_file('data.txt')); // emits data from the file in blocks
stream_pipe(stream : pipe_md5());                 // calculates the md5 for the data block
stream_sink(stream : %paddr(writeMd5));           // local procedure storing the data

// start the stream , let the data flow
stream_flow(stream);

// cleanup the mess =)
stream_dispose(stream);
```

This project was inspired by the Streaming API of Java and Node.js.


## Installation ##
The easiest way to install this is to use the iPKG client: 

```
ipkg install stream
```

and you are done =) .

Pipes and emitter are packaged in their own packages.


## Building ##
The project can be build with `make`. The following parameter can be set:

- BIN_LIB : library for the created objects
- BIND_LIB : library where the dependencies can be found (ARRAYLIST, MESSAGE)
- TGTRLS : Target Release, f. e. V7R3M0
- INCDIR : folder where the copybooks are (f. e. /usr/local/include)

So your command may look like:

    make BIN_LIB=MIHAEL INCDIR=/home/mihael/include compile

Other targets are `bind` and `clean`.


## Contribution guidelines ##
You can contribute in any way you like. Just do it! =)

Take a look at the wiki to get into the project.


## Who do I talk to? ##
Mihael Schmidt , mihael@rpgnextgen.com