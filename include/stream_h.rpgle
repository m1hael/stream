**FREE

/if not defined(STREAM)
/define STREAM

///
// Stream : Streaming API for ILE
//
// A stream consist of an emitter which provides the base data for the stream. 
// The data can be piped through pipes. Those pipes can do various things<br/>
// <br/>
// <ul>
//   <li>inspect the data, react on it and push it further down to the next pipe</li>
//   <li>drop the data and don't push it to the next pipe</li>
//   <li>create a different kind of data and push it to the next pipe</li>
//   <li>create multiple data of the same or different kind and push it to the next pipe</li>
// </ul>
// 
// And after all the inspecting, reacting and transforming it can be drained in a
// sink which is the end of the road for the data. The sink does the end processing
// of the data. Example:<br/>
// <br/>
// <ol>
//   <li>Emitter: reads blocks of data from a file and passes it down to the next pipe</li>
//   <li>Pipe: calculates MD5 hash of that block of data but drops the data till it has
//             received all blocks and passes the MD5 hash down to the next pipe or sink</li>
//   <li>Sink: writes the hash to a database table</li>
// </ol>
// 
// The code could look like this:
// 
// <pre>
// // setup the stream
// stream = stream_create(emitter_file('data.txt')); // emits data from the file in blocks
// stream_pipe(stream : pipe_md5());                 // calculates the md5 for the data block
// stream_sink(stream : %paddr(writeMd5());          // local procedure storing the md5 hash
// 
// // start the stream , let the data flow
// stream_flow(stream);
// 
// // cleanup the mess =)
// stream_dispose(stream);
// </pre>
// 
// This project was inspired by the Streaming API of Java and Node.js.
//
// @author Mihael Schmidt
// @date 02.05.2020
// @project Stream
// @version 1.0.0
// @link https://bitbucket.org/m1hael/stream/ Stream project website
///

///
// Emitter
// 
// Emitter must implement at least this data structure. The first field must be
// a pointer to the emit procedure and the second field a pointer to the dispose 
// procedure. The implementation can add field to this data structure as needed. 
/// 
dcl-ds stream_emitter_t qualified template;
  emit pointer(*proc);
  dispose pointer(*proc);
end-ds;

///
// Error handler
//
// Error handler must implement at least this data structure. The implementation
// can extend the data structure as needed.
///
dcl-ds stream_errorHandler_t qualified template;
  handleError pointer(*proc);
  dispose pointer(*proc);
end-ds;

///
// Pipe
//
// Pipe handler must implement at least this data structure. The implementation
// can extend the data structure as needed.
///
dcl-ds stream_pipe_t qualified template;
  pipe pointer(*proc);
  dispose pointer(*proc);
end-ds;

///
// Event type signaling a data event.
///
dcl-c STREAM_EVENT_DATA 1;
///
// Event type signaling the end of the stream. The data field of the event holds
// no data but *null as a value.
///
dcl-c STREAM_EVENT_END -1;

///
// Stream Event
//
// This data strucutre will be created by the emitter and passed down the pipes
// to the sink. If it is a STREAM_EVENT_DATA event then it holds a pointer to 
// the data and the length of the data in bytes.
//
// This data structure is also used to signal that there is no more data and the
// stream ended. Every pipe or sink which had to do some grouping or post processing
// should do so now. Type will then contain the STREAM_EVENT_END constant value.
///
dcl-ds stream_event_t qualified template;
  type int(10);
  data pointer;
  length int(10);
end-ds;

///
// Create stream
//
// Create stream from the parameters passed to this procedure. The emitter
// must be passed as a parameter to this constructing procedure as it is the
// base of the stream. The pipes and sink can be added later as needed. The
// number of pipes passed here as parameters are just for convenience. More 
// than 9 pipes can be added to the stream. The limit is the memory limit of 
// the job.
// <p>
// The stream is created with the following defaults:
// <ul>
//   <li>Auto deallocate resources = *on</li>
//   <li>Auto deallocate temporary memory = *off</li>
//   <li>Send STREAM_EVENT_END event = *off</li>
// </ul>
//
// @param Emitter
// @param Error handler
// @param Pipe (1)
// @param Pipe (2)
// @param Pipe (3)
// @param Pipe (4)
// @param Pipe (5)
// @param Pipe (6)
// @param Pipe (7)
// @param Pipe (8)
// @param Pipe (9)
//
// @return Pointer to the constructed stream
///
dcl-pr stream_create pointer extproc(*dclcase);
  emitter pointer const options(*omit : *nopass);
  errorHandler pointer const options(*omit : *nopass);
  pPipe1 pointer options(*nopass) const;
  pPipe2 pointer options(*nopass) const;
  pPipe3 pointer options(*nopass) const;
  pPipe4 pointer options(*nopass) const;
  pPipe5 pointer options(*nopass) const;
  pPipe6 pointer options(*nopass) const;
  pPipe7 pointer options(*nopass) const;
  pPipe8 pointer options(*nopass) const;
  pPipe9 pointer options(*nopass) const;
end-pr;

///
// Set emitter
//
// Sets the emitter for this stream. Any previously set emitter will be replaced.
//
// @param Stream
// @param Emitter
///
dcl-pr stream_emitter extproc(*dclcase);
  stream pointer const;
  emitter pointer const;
end-pr;
  
///
// Append pipe
//
// Append a pipe to the processing pipeline of the stream.
//
// @param Stream
// @param Pipe
///
dcl-pr stream_pipe extproc(*dclcase);
  stream pointer const;
  pipe pointer const;
end-pr;

///
// Push event
//
// Pushes the event to the next pipe or sink. Only emitters and pipes can push
// events down to the next stage. Sinks cannot push events any further as there
// is nothing after a sink.
//
// @param Stream
// @param Event
///
dcl-pr stream_push extproc(*dclcase);
  stream pointer const;
  event likeds(stream_event_t) const;
end-pr;

///
// Set sink
//
// Sets the sink to the stream. Replaces any previously set sink.
//
// @param Stream
// @param Sink
///
dcl-pr stream_sink extproc(*dclcase);
  stream pointer const;
  sink pointer(*proc) const;
end-pr;

///
// Dispose stream
//
// Frees any resources associated with this stream. If any memory has been 
// allocated by the <em>stream_alloc</em> procedure it will also be freed.
//
// If the option <em>autoDeallocate</em> has been set then the memory of the
// set emitter, error handler, pipes and sink will also be released. If either 
// of them provide a <em>dispose</em> procedure it will be called prior to
// freeing the memory for the data structure.
//
// @param Stream
///
dcl-pr stream_dispose extproc(*dclcase);
  stream pointer;
end-pr;

///
// Start flow
//
// Starts the flow of the stream. The emitter will be called for data till
// the emitter returns an STREAM_EVENT_END event. The event will be processed
// by anything further down till the event is either dropped or sunk.
// 
// @param Stream
///
dcl-pr stream_flow extproc(*dclcase);
  stream pointer const;
end-pr;

///
// Allocate memory
//
// Allocates a block of memory by the given size. This memory will be freed
// when the stream ends.
//
// TODO document autoTempDeallocate
//
// @param Stream
// @param Size of the requested memory
//
// @return Pointer to newly allocated memory
///
dcl-pr stream_alloc pointer extproc(*dclcase);
  stream pointer const;
  size uns(10) const;
end-pr;

///  
// Set auto deallocate of resources  
//  
// Sets the option to automatically free any resources associated with this
// stream on the <em>stream_dispose</em> call. Associated resources can be: 
// emitter, error handler, pipe, allocated memory via <em>stream_allocate</em>.
// 
// @param Stream
// @param *on = automatically deallocate resources on <em>stream_dispose</em>
///  
dcl-pr stream_autoDeallocate extproc(*dclcase);
  stream pointer const;
  autoDeallocate ind const;
end-pr;

///
// Set automatically deallocate temporary memory
//
// If this option is set then every memory allocated after the emitter emits
// a data block is freed at the end of processing this block of data. This is
// handy for resources allocating memory for processing the data or transforming
// the data into a new data value set in the event data strucutre.
//
// @param Stream
// @param *on = automatically free memory after processing of data event
///
dcl-pr stream_autoTempDeallocate extproc(*dclcase);
  stream pointer const;
  autoTempDeallocate ind const;
end-pr;

/// 
// Send end event
//
// If this option is set then the STREAM_EVENT_END event will also be send to
// the pipes and sink.
//
// This comes in handy to tell the pipes and sink that there will be no more
// data and the stream ended. This is the time to finish things like grouping
// or summing up values.
//
// @param Stream
// @param *on = send STREAM_EVENT_END event to pipes and sink
///
dcl-pr stream_sendEnd extproc(*dclcase);
  stream pointer const;
  sendEnd ind const;
end-pr;

///
// Plug emitter
//
// Calling this procedure causes the data flow to stop at the emitter. All 
// "flowing" data will flow through the pipeline to the sink but there will be 
// no new data blocks emitted.
//
// @param Stream
///
dcl-pr stream_plug extproc(*dclcase);
  streamPtr pointer const;
end-pr;

/endif