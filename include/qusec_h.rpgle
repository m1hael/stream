**FREE

/if not defined(QUSEC)
/define QUSEC

dcl-ds qusec_t qualified template;
  bytesProvided int(10);
  bytesAvailable int(10);
  exceptionId char(7);
  reserved char(1);
end-ds;

/endif