///
// Stream : Streaming API for ILE
//
// A stream consist of an emitter which provides the base data for the stream. 
// The data can be piped through pipes. Those pipes can do various things<br/>
// <br/>
// <ul>
//   <li>inspect the data, react on it and push it further down to the next pipe</li>
//   <li>drop the data and don't push it to the next pipe</li>
//   <li>create a different kind of data and push it to the next pipe</li>
//   <li>create multiple data of the same or different kind and push it to the next pipe</li>
// </ul>
// 
// And after all the inspecting, reacting and transforming it can be drained in a
// sink which is the end of the road for the data. The sink does the end processing
// of the data. Example:<br/>
// <br/>
// <ol>
//   <li>Emitter: reads blocks of data from a file and passes it down to the next pipe</li>
//   <li>Pipe: calculates MD5 hash of that block of data but drops the data till it has
//             received all blocks and passes the MD5 hash down to the next pipe or sink</li>
//   <li>Sink: writes the hash to a database table</li>
// </ol>
// 
// The code could look like this:
// 
// <pre>
// // setup the stream
// stream = stream_create();                          // create a stream
// stream_emitter(stream : emitter_file('data.txt')); // stream emits data from a file in blocks
// stream_pipe(stream : pipe_md5());                  // calculates the md5 for the data block
// stream_sink(stream : %paddr(writeMd5());           // local procedure for storing the md5 hash
// 
// // start the stream , let the data flow
// stream_flow(stream);
// 
// // cleanup the mess =)
// stream_dispose(stream);
// </pre>
// 
// This project was inspired by the Streaming API of Java and Node.js.
//
// @author Mihael Schmidt
// @date 02.05.2020
// @project Stream
// @version 1.0.0
// @link https://bitbucket.org/m1hael/stream/ Stream project website
///