**FREE

/if not defined(STREAMEDBL)
/define STREAMEDBL

///
// Stream : Double Emitter
//
// This module provides a configurable double emitter.
//
// @author Mihael Schmidt
// @date 06.05.2020
// @project Stream
// @link https://bitbucket.org/m1hael/stream/ Stream project website
///


///
// Create Double Emitter
//
// Creates an emitter which emits double values.
// <p>
// If a stream is passed to the procedure the memory for the emitter will be
// allocated with the <em>stream_allocate</em> procedure. The memory will be
// released with the <em>stream_dispose</em> procedure.
// <p>
// Defaults for the emitter are:
// <ul>
//   <li>Start value: 1.0</li>
//   <li>End value: *HIVAL</li>
//   <li>Incrementation value: 1.0</li>
// </ul>
//
// @param Stream
// @param Start value
// @param End value
// @param Incrementation value
//
// @return Event with double value
//
// @info If no stream is passed to this procedure it allocates memory for
//       storing the state of the emitter. This memory must be freed after
//       the stream ends by calling <em>dealloc</em> on the returned pointer.
///
dcl-pr stream_emitter_double pointer extproc(*dclcase);
  stream pointer const options(*omit : *nopass);
  start float(8) const options(*nopass);
  endValue float(8) const options(*nopass);
  increment float(8) const options(*nopass);
end-pr;

/endif