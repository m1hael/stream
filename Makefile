#
# Stream : Streaming API for ILE
#

#-------------------------------------------------------------------------------
# User-defined part start
#

# BIN_LIB is the destination library
BIN_LIB=QGPL

TGTRLS=*CURRENT

INCDIR=/usr/local/include

VERSION=1.0.0

#
# User-defined part end
#-------------------------------------------------------------------------------


all: compile bind commands

compile:
	$(MAKE) -C src/stream/ compile $*
	$(MAKE) -C src/emitter/ compile $*

clean:
	$(MAKE) -C src/stream/ clean $*
	$(MAKE) -C src/emitter/ clean $*

bind:
	$(MAKE) -C src/stream/ bind $*
	$(MAKE) -C src/emitter/ bind $*

release: prerelease
	-system "DLTOBJ $(BIN_LIB)/STREAMSAVF OBJTYPE(*FILE)"
	system "CRTSAVF $(BIN_LIB)/STREAMSAVF"
	system "CHGOWN OBJ('/QSYS.LIB/$(BIN_LIB).LIB/STREAM.SRVPGM') NEWOWN(QPGMR)"
	system "CHGOWN OBJ('/QSYS.LIB/$(BIN_LIB).LIB/STREAMEMIT.SRVPGM') NEWOWN(QPGMR)"
	system "SAVOBJ OBJ(STREAM) LIB($(BIN_LIB)) DEV(*SAVF) OBJTYPE(*SRVPGM) SAVF($(BIN_LIB)/STREAMSAVF) TGTRLS($(TGTRLS)) DTACPR(*YES)"
	system "SAVOBJ OBJ(STREAMEMIT) LIB($(BIN_LIB)) DEV(*SAVF) OBJTYPE(*SRVPGM) SAVF($(BIN_LIB)/STREAMSAVF) TGTRLS($(TGTRLS)) DTACPR(*YES)"
	
.PHONY:
